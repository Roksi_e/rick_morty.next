/* eslint-disable @next/next/no-img-element */
import Image from 'next/image'
import Link from 'next/link'

export default function Home() {
	return (
		<div className="base-bg w-full h-full bg-fondo flex flex-col justify-around gap-8 relative">
			<div className=" w-[100%] h-32 relative flex items-center  flex-col  z-10 ">
				<img src={'/images/group.png'} alt="logo" />
			</div>
			<div className="z-10 w-[100%] h-[60vh] relative flex justify-center my-auto py-14">
				<Link href="/planet">
					<Image src={'/images/ufo.png'} alt="ufo" priority width={500} height={500} className="h-auto" />
				</Link>
			</div>
		</div>
	)
}
