'use client'

export default function ErrorCharacters({ error }: { error: Error }) {
	return (
		<div className="flex justify-center items-center text-xl text-white relative">
			<h1 className="text-white">Oops!!! {error.message}</h1>
		</div>
	)
}
