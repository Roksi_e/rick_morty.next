import Header from '@/components/ui/header'
import Providers from '@/lib/providers'
import { Metadata } from 'next'

export const metadata: Metadata = {
	title: 'Characters'
}

export default function CharacterLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="base-bg w-full h-full bg-cielo min-h-[80vh] flex flex-col ">
			<Providers>
				<Header />
				<section className="lg:p-2 sx:pt-32 lg:mt-0 sx:mt-12 sx:pb-12 w-full min-h-[100vh] flex flex-col items-center">
					{children}
				</section>
			</Providers>
		</div>
	)
}
