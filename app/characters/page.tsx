'use client'

import React, { useState } from 'react'
import { useQuery } from '@tanstack/react-query'
import CharacterCard from '@/components/ui/characterCard'
import { getDataCharacter } from '@/services/api'
import { Button } from '@/components/ui/button'
import { ChevronLeft, ChevronRight } from '@/components/icons'

export default function CharactersPage() {
	const [pageCharacter, setPageCharacter] = useState(1)
	const [currentPage, setCurrentPage] = useState(0)
	const itemsPerPage: number = 3

	const {
		isLoading,
		isError,
		isPreviousData,
		data: characters
	} = useQuery({
		queryKey: ['characters', pageCharacter],
		queryFn: () => getDataCharacter(pageCharacter),
		keepPreviousData: true
	})

	const startIndex: number = currentPage * itemsPerPage
	const endIndex: number = startIndex + itemsPerPage
	const currentCharacters = characters?.results.slice(startIndex, endIndex)

	const handleNextPage = () => {
		setCurrentPage(currentPage + 1)
	}

	const handlePrevPage = () => {
		setCurrentPage(currentPage - 1)
	}

	return (
		<React.Fragment>
			{isLoading ? (
				<div className="text-white">Loading...</div>
			) : isError ? (
				<div className="text-white">Something went wrong...</div>
			) : (
				<div className="px-8 w-full flex lg2:flex-row sx:flex-col justify-center items-center gap-8">
					<div className="lg2:w-[10%]">
						{currentPage !== 0 ? (
							<ChevronLeft
								onClick={handlePrevPage}
								className="lg2:block sx:hidden  text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
							/>
						) : (
							<ChevronLeft className="lg2:block sx:hidden text-secondary text-9xl w-36 h-36 z-40" />
						)}
					</div>

					<div className="lg:w-[80%] sx:w-full flex lg:flex-row sx:flex-col justify-center items-center gap-3 ">
						{currentCharacters?.map(char => {
							return (
								<div key={char.id} className="lg:w-[33%] sx:w-full lg:pt-[115px] sx:pt-12 flex justify-center">
									<CharacterCard
										id={char.id}
										status={char.status}
										type={char.type}
										gender={char.gender}
										species={char.species}
										image={char.image}
										name={char.name}
										origin={char.origin}
										location={char.location}
										episode={char.episode}
										url={char.url}
										created={char.created}
									/>
								</div>
							)
						})}
					</div>
					<div className="lg2:w-[10%]">
						{endIndex >= characters?.results.length ? (
							<ChevronRight className="lg2:block sx:hidden text-secondary text-9xl w-36 h-36 z-40" />
						) : (
							<ChevronRight
								onClick={handleNextPage}
								className="lg2:block sx:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
							/>
						)}
					</div>

					<div className="w-full sx:flex lg2:hidden  justify-between items-center">
						{currentPage !== 0 ? (
							<ChevronLeft
								onClick={handlePrevPage}
								className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
							/>
						) : (
							<ChevronLeft
								className={
									currentCharacters?.length !== 0
										? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
										: 'invisible'
								}
							/>
						)}
						{endIndex >= characters?.results.length ? (
							<ChevronRight
								className={
									currentCharacters?.length !== 0
										? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
										: 'invisible'
								}
							/>
						) : (
							<ChevronRight
								onClick={handleNextPage}
								className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
							/>
						)}
					</div>
				</div>
			)}
			<div className="flex justify-center gap-[10px] m-5">
				<Button
					onClick={() => setPageCharacter(old => Math.max(old - 1, 0))}
					disabled={pageCharacter <= 1}
					className="text-white"
				>
					<ChevronLeft />
				</Button>
				<Button variant="ghost">{pageCharacter}</Button>
				<Button
					onClick={() => {
						if (!isPreviousData && characters?.info.pages) {
							setPageCharacter(old => old + 1)
						}
					}}
					disabled={isPreviousData || !characters?.info.next}
				>
					<ChevronRight />
				</Button>
			</div>
		</React.Fragment>
	)
}
