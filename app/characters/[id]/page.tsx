'use client'

import React from 'react'
import { useQuery } from '@tanstack/react-query'
import Image from 'next/image'
import { getDataCharacterById } from '@/services/api'

export default function CharacterPage({ params }: { params: { id: number } }) {
	const {
		data: character,
		isLoading,
		isError
	} = useQuery({
		queryKey: ['character', params.id],
		queryFn: () => getDataCharacterById(params.id)
	})

	if (isLoading) {
		return <h1>Loading</h1>
	}
	if (isError) {
		return <h1>Oops!!!</h1>
	}

	return (
		<div className="w-full min-h-[80vh] flex justify-center items-center m-auto">
			<div className="relative flex lg:flex-row sx:flex-col gap-[40px] justify-center items-center w-full max-w-[960px] lg:p-[40px] sx:p-4 mx-auto bg-white rounded-[40px]">
				<Image
					src={
						character?.image ? character.image : `https://rickandmortyapi.com/api/character/avatar/${params.id}.jpeg`
					}
					alt="character"
					priority
					width={463}
					height={463}
					className="border-2 border-black rounded-[20px] sx:mt-32 lg:mt-0 object-contain "
				/>

				<div className="lg:min-w-[304px] sx:w-full border border-black rounded-lg py-4 lg:px-12 sx:px-4 bg-positive lg:text-lg sx:text-xl">
					<div className="mb-2 lg:text-5xl sx:text-2xl border-b-2 border-black font-bold p-4 w-[80%]">
						{character?.name}
					</div>
					<div className="mb-2">
						<strong>Status:</strong> {character?.status}
					</div>
					<div className="mb-2">
						<strong>Species:</strong> {character?.species}
					</div>
					<div className="mb-2">
						<strong>Type:</strong> {character?.type ? character?.type : `""`}
					</div>
					<div className="mb-2">
						<strong>Gender:</strong> {character?.gender}
					</div>
					<div className="mb-2">
						<strong>Origin:</strong> {character?.origin.name}
					</div>
				</div>
				<Image
					src={'/images/planet-7 5.png'}
					alt="planet7"
					priority
					style={{ objectFit: 'cover' }}
					width={250}
					height={250}
					className="h-auto border rounded-[50%] absolute lg:top-[-35%] sx:top-[-18%] lg:right-[-10%]  shadow-[0_0px_47px_15px_rgba(202,250,130,1)]"
				/>
			</div>
		</div>
	)
}
