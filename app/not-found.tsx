'use client'

import Link from 'next/link'
import Image from 'next/image'
import Picle from '@/public/images/picle.png'
import Header from '@/components/ui/header'

export default function NotFound() {
	return (
		<div className="base-bg w-full h-full bg-cielo flex flex-col">
			<Header />
			<div className="w-full min-h-[80%] flex justify-center flex-col items-center lg2:gap-10 sx:gap-2 text-white relative lg2:py-20 sx:py-12 lg2:px-24 sx:px-4">
				<h1 className=" lg2:pt-[62px] mx-auto lg2:text-[104px] sx:text-5xl font-bold text-center">Error 404</h1>
				<div className="w-full flex lg2:flex-row sx:flex-col justify-between items-center mx-auto">
					<p className=" inline-block lg2:w-[60%] sx:w-full lg2:text-6xl sx:text-2xl font-semibold  tracking-[0.5rem]">
						Don’t worry my friend, not an alien penis... Flip the pickle.
					</p>
					<div className="lg2:w-[259px] sx:w-[168px] sx:mt-10 lg:mt-0">
						<Link href="/">
							<Image src={Picle} alt="photo" sizes="259" />
						</Link>
					</div>
				</div>
			</div>
		</div>
	)
}
