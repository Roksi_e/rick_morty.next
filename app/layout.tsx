import type { Metadata } from 'next'
import { IBM_Plex_Mono } from 'next/font/google'
import Footer from '@/components/ui/footer'
import Providers from '@/lib/providers'
import '@/styles/globals.css'

const plexMono = IBM_Plex_Mono({
	subsets: ['cyrillic'],
	weight: '400'
})

export const metadata: Metadata = {
	title: 'Rick and Morty',
	description: 'Generated by create next app'
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
	return (
		<html lang="ua" data-theme="light">
			<body className={plexMono.className}>
				<Providers>
					<div className="relative w-full flex flex-col min-h-screen ">
						<main className="w-full flex-auto min-h-[80%] relative">{children}</main>
						<Footer />
					</div>
				</Providers>
			</body>
		</html>
	)
}
