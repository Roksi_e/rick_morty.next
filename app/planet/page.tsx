'use client'

import React, { useState } from 'react'
import { useQuery } from '@tanstack/react-query'
import PlanetCard from '@/components/ui/planetCard'
import { getDataPlanet } from '@/services/api'
import { Button } from '@/components/ui/button'
import { ChevronLeft, ChevronRight } from '@/components/icons'

export default function PlanetsPage() {
	const [page, setPage] = useState(1)

	const {
		isLoading,
		isError,
		isPreviousData,
		data: planets
	} = useQuery({
		queryKey: ['planets', page],
		queryFn: () => getDataPlanet(page),
		keepPreviousData: true
	})

	return (
		<React.Fragment>
			<div className="h-full  flex flex-col justify-center gap-[30px] max-w-[1200px] mt-4 mx-auto">
				{isLoading ? (
					<div className="text-white">Loading...</div>
				) : isError ? (
					<div className="text-white">Something went wrong...</div>
				) : (
					<div className=" grid lg:grid-cols-5 sx:grid-cols-2  relative w-[100%] min-h-[70vh] mx-auto">
						{planets?.results.map(planet => (
							<div key={planet.id}>
								<PlanetCard
									id={planet.id}
									name={planet.name}
									type={planet.type}
									dimension={planet.dimension}
									residents={planet.residents}
									url={planet.url}
									created={planet.created}
								></PlanetCard>
							</div>
						))}
					</div>
				)}
				<div className="flex justify-center gap-[10px] m-5">
					<Button onClick={() => setPage(old => Math.max(old - 1, 0))} disabled={page <= 1} className="text-white">
						<ChevronLeft size="24" />
					</Button>
					<Button variant="ghost">{page}</Button>
					<Button
						onClick={() => {
							if (!isPreviousData && planets?.info.pages) {
								setPage(old => old + 1)
							}
						}}
						disabled={isPreviousData || !planets?.info.next}
					>
						<ChevronRight size="24" />
					</Button>
				</div>
			</div>
		</React.Fragment>
	)
}
