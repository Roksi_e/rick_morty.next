'use client'

import React, { useState } from 'react'
import { useQuery } from '@tanstack/react-query'
import Image from 'next/image'
import { getDataPlanetById } from '@/services/api'
import Character from '@/components/ui/character'
import { ChevronLeft, ChevronRight } from '@/components/icons'

export default function LocationPage({ params }: { params: { id: number } }) {
	const [currentPage, setCurrentPage] = useState(0)
	const itemsPerPage: number = 3

	const {
		data: planet,
		isLoading,
		isError
	} = useQuery({
		queryKey: ['planet', params.id],
		queryFn: () => getDataPlanetById(params.id)
	})

	const startIndex: number = currentPage * itemsPerPage
	const endIndex: number = startIndex + itemsPerPage
	const currentCharacters = planet?.residents?.slice(startIndex, endIndex)

	const handleNextPage = () => {
		setCurrentPage(currentPage + 1)
	}

	const handlePrevPage = () => {
		setCurrentPage(currentPage - 1)
	}

	if (isLoading) {
		return <h1>Loading...</h1>
	}
	if (isError) {
		return <h1>Oops!!!Something went wrong...</h1>
	}

	return (
		<React.Fragment>
			<div className="flex lg:flex-row sx:flex-col gap-8 lg:justify-end sx:justify-center sx:items-center lg:relative w-full lg:h-[50vh] sx:min-h-[50vh] lg:m-10 sx:m-0 lg:p-10 sx:p-4">
				<div className="  ">
					<Image
						src={'/images/planet-7 5.png'}
						alt="planet7"
						priority
						width={500}
						height={500}
						className="lg:absolute top-0 lg:left-[-200px]  object-contain  h-auto lg:w-[507px] sx:w-[200px] "
					/>
				</div>
				<div className="text-white lg:text-4xl sx:text-2xl lg:w-[60%] sx:w-full ">
					<strong>Planet:</strong>
					<p className="lg:text-4xl sx:text-2xl ml-[36px]">&#8226; {planet?.name}</p>
					<strong>Type:</strong>
					<p className="lg:text-4xl sx:text-2xl ml-[36px]">&#8226; {planet?.type}</p>
					<strong>Dimension:</strong>
					<p className="lg:text-4xl sx:text-2xl ml-[36px]">&#8226; {planet?.dimension}</p>
					<strong>Created:</strong>
					<p className="lg:text-4xl sx:text-2xl ml-[36px]">&#8226; {planet?.created}</p>
				</div>
			</div>
			<div className=" mb-8 flex lg2:flex-row sx:flex-col justify-center items-center gap-8 w-full px-8 min-h-80">
				<div className="lg2:w-[15%]">
					{currentPage !== 0 ? (
						<ChevronLeft
							onClick={handlePrevPage}
							className="lg2:block sx:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					) : (
						<ChevronLeft
							className={
								currentCharacters?.length !== 0
									? 'lg2:block sx:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					)}
				</div>

				{planet?.residents ? (
					<div className="lg:w-[80%] sx:w-full flex lg:flex-row sx:flex-col justify-center items-center gap-3">
						{currentCharacters?.map(char => {
							const arr = char.split('/').filter(Boolean)
							const id = parseInt(arr[arr.length - 1])
							return (
								<div key={id} className="lg:w-[33%] sx:w-full lg:pt-[115px] sx:pt-12 flex justify-center">
									<Character id={id}></Character>
								</div>
							)
						})}
					</div>
				) : null}
				<div className="lg2:w-[15%]">
					{endIndex >= planet?.residents?.length ? (
						<ChevronRight
							className={
								currentCharacters?.length !== 0
									? 'lg2:block sx:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					) : (
						<ChevronRight
							onClick={handleNextPage}
							className="lg2:block sx:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					)}
				</div>

				<div className="w-full sx:flex lg2:hidden  justify-between items-center">
					{currentPage !== 0 ? (
						<ChevronLeft
							onClick={handlePrevPage}
							className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					) : (
						<ChevronLeft
							className={
								currentCharacters?.length !== 0
									? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					)}
					{endIndex >= planet?.residents?.length ? (
						<ChevronRight
							className={
								currentCharacters?.length !== 0
									? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					) : (
						<ChevronRight
							onClick={handleNextPage}
							className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					)}
				</div>
			</div>
		</React.Fragment>
	)
}
