import Header from '@/components/ui/header'
import Providers from '@/lib/providers'
import { Metadata } from 'next'

export const metadata: Metadata = {
	title: 'Planets'
}

export default function PlanetLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="base-bg w-full h-full bg-cielo min-h-[80vh] flex flex-col ">
			<Providers>
				<Header />
				<section className="p-2 w-full min-h-[100vh]">{children}</section>
			</Providers>
		</div>
	)
}
