import Header from '@/components/ui/header'
import Providers from '@/lib/providers'
import { Metadata } from 'next'

export const metadata: Metadata = {
	title: 'Favorites'
}

export default function FavoritesLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="base-bg w-full h-full bg-cielo min-h-[80vh] flex flex-col ">
			<Providers>
				<Header />
				<section className="w-full min-h-[80vh]">{children}</section>
			</Providers>
		</div>
	)
}
