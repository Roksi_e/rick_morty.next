/* eslint-disable react/no-unescaped-entities */
'use client'

import React, { useEffect, useState } from 'react'
import { useQuery } from '@tanstack/react-query'
import Image from 'next/image'
import { useAddFavorite } from '@/data/store'
import { getMultipleCharacters } from '@/services/api'
import Character from '@/components/ui/character'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/components/ui/select'
import SearchPlanet from '@/components/ui/searchPlanet'

export default function FavoritesPage() {
	const { favorites } = useAddFavorite()
	const [filter, setFilter] = useState('')
	const [planets, setPlanets] = useState<string[]>([])

	const {
		isLoading,
		isError,
		data: characters
	} = useQuery({
		queryKey: ['characters', favorites],
		queryFn: () => getMultipleCharacters(favorites),
		keepPreviousData: true
	})

	useEffect(() => {
		if (characters?.length) {
			const uniquePlanets: string[] = [...new Set(characters?.map(({ origin }) => origin.name))]
			setPlanets(uniquePlanets)
		}
	}, [characters])

	return (
		<div className="relative w-full min-h-[80vh] flex flex-col gap-4 justify-between lg:px-8 py-8 sx:px-2">
			<div className="lg:w-[50vw] sx:w-full h-[175px] mx-auto relative">
				<Image src={'/images/favorites 1.png'} alt="favorites" priority fill sizes="550" />
			</div>
			<div className="max-w-[400px] w-full h-[70px] mx-auto">
				<Select defaultValue={planets[0]} onValueChange={value => setFilter(value)}>
					<SelectTrigger className="max-w-[400px] w-full">
						<SelectValue placeholder="Search by Planet" />
					</SelectTrigger>
					{favorites?.length && (
						<SelectContent position="popper">
							{planets?.map(planet => (
								<SelectItem key={planet} value={planet}>
									{planet}
								</SelectItem>
							))}
						</SelectContent>
					)}
				</Select>
			</div>

			{isLoading ? (
				<div className="text-white">Loading...</div>
			) : isError ? (
				<div className="text-white">Something went wrong...</div>
			) : (
				<div
					className={`mx-auto lg:w-[80vw] sx:w-full flex flex-row items-center gap-8 ${
						favorites?.length <= 1 ? 'justify-center' : 'lg:justify-between sx:justify-center'
					}`}
				>
					<React.Fragment>
						{favorites?.length !== 0 && Array.isArray(characters) ? (
							<SearchPlanet characters={characters} filter={filter} />
						) : favorites?.length === 1 ? (
							<div className="lg:pt-[115px] sx:pt-12 flex justify-center items-center">
								<Character id={favorites[0]} />
							</div>
						) : (
							<div className=" text-foreground h-80 lg:text-3xl sx:text-xl2 text-center my-20 lg:w-[70%] sx:w-full">
								There doesn't seem to be anything to show here currently, if you see a character you want to add to
								favorites, hit the white star so you can see them here, change their name and assign them a rating!
							</div>
						)}
					</React.Fragment>
				</div>
			)}
		</div>
	)
}
