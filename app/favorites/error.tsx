'use client'

export default function ErrorFavorites({ error }: { error: Error }) {
	return (
		<div className="flex justify-center items-center text-xl text-white relative">
			<h1 className="text-white">Oops!!! {error.message}</h1>
		</div>
	)
}
