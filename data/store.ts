import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

interface AddToFavorite {
	favorites: number[]
	addToFav: (id: number) => void
	deleteToFav: (id: number) => void
}

export const useAddFavorite = create<AddToFavorite>()(
	persist(
		(set, get) => ({
			favorites: [],

			addToFav: id => set(state => ({ favorites: [...state.favorites, id] })),

			deleteToFav: (id: number) => {
				const { favorites } = get()
				set({
					favorites: favorites.filter(char => char !== id)
				})
			}
		}),
		{
			name: 'favorites',
			storage: createJSONStorage(() => localStorage)
		}
	)
)
