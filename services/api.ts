import axios from 'axios'

axios.defaults.baseURL = 'https://rickandmortyapi.com/api'

export const getDataPlanet = async (
	page: number = 1
): Promise<{
	info: Info
	results: ILocation[]
}> => {
	try {
		const res = await axios.get(`/location?page=${page}`)
		return res.data
	} catch (error) {
		throw new Error('Failed to fetch data')
	}
}

export const getDataPlanetById = async (id: number): Promise<ILocation> => {
	try {
		const res = await axios.get(`/location/${id}`)
		return res.data
	} catch (error) {
		throw new Error('Failed to fetch data')
	}
}

export const getDataCharacterById = async (id: number): Promise<Character> => {
	try {
		const res = await axios.get(`/character/${id}`)
		return res.data
	} catch (error) {
		throw new Error('Failed to fetch data')
	}
}

export const getDataCharacter = async (
	page: number = 1
): Promise<{
	info: Info
	results: Character[]
}> => {
	try {
		const res = await axios.get(`/character?page=${page}`)
		return res.data
	} catch (error) {
		throw new Error('Failed to fetch data')
	}
}

export const getMultipleCharacters = async (id: number[]): Promise<Character[]> => {
	try {
		const res = await axios.get(`/character/${id}`)

		return res.data
	} catch (error) {
		throw new Error('Failed to fetch data')
	}
}
