import planet1 from '@/public/images/planet-1 4.png'
import planet2 from '@/public/images/planet-2 11.png'
import planet3 from '@/public/images/planet-3 3.png'
import planet4 from '@/public/images/planet-4 2.png'
import planet5 from '@/public/images/planet-5 2.png'
import planet6 from '@/public/images/planet-6 3.png'
import planet7 from '@/public/images/planet-7 5.png'
import planet8 from '@/public/images/planet-8 3.png'
import planet9 from '@/public/images/planet-9 4.png'
import planet10 from '@/public/images/planet-10 3.png'
import planet11 from '@/public/images/planet-11 4.png'
import planet12 from '@/public/images/planet-12 3.png'
import planet13 from '@/public/images/planet-13 3.png'
import planet14 from '@/public/images/planet-14 3.png'
import planet15 from '@/public/images/planet-15 4.png'

export const planetsAll = [
	planet1,
	planet2,
	planet3,
	planet4,
	planet5,
	planet6,
	planet7,
	planet8,
	planet9,
	planet10,
	planet11,
	planet12,
	planet13,
	planet14,
	planet15
]
