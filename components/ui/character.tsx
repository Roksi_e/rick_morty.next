'use client'

import React from 'react'
import { useQuery } from '@tanstack/react-query'
import { getDataCharacterById } from '@/services/api'
import CharacterCard from './characterCard'

export default function Character({ id }: { id: number }) {
	const {
		data: character,
		isLoading,
		isError
	} = useQuery({
		queryKey: ['character', id],
		queryFn: () => getDataCharacterById(id)
	})

	if (isLoading) {
		return <h1>Loading...</h1>
	}
	if (isError) {
		return <h1>Oops!!!Something went wrong...</h1>
	}

	return (
		<React.Fragment>
			{character ? (
				<CharacterCard
					id={character?.id}
					status={character?.status}
					type={character?.type}
					gender={character?.gender}
					species={character?.species}
					image={character?.image}
					name={character?.name}
					origin={character?.origin}
					location={character?.location}
					episode={character?.episode}
					url={character?.url}
					created={character?.created}
				></CharacterCard>
			) : null}
		</React.Fragment>
	)
}
