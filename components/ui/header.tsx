/* eslint-disable @next/next/no-img-element */
'use client'

import Link from 'next/link'
import { useState } from 'react'
import { Close } from '../icons'

const Header = () => {
	const [isOpen, setIsOpen] = useState(false)

	return (
		<>
			<header className="w-full bg-primary h-[80px] flex lg:justify-end sx:justify-between p-2 relative z-10 bg-transparent">
				<div className="h-[100%] lg:w-[40%] sx:w-[80%] flex items-center justify-center  p-2">
					<img className="w-full h-auto object-contain" src={'/images/group.png'} alt="navlogo" />
				</div>
				<div className="lg:hidden sx:block cursor-pointer" onClick={() => setIsOpen(!isOpen)}>
					<img src={'/images/group 4.png'} alt="menu" />
				</div>

				<ul className="lg:flex sx:hidden justify-end items-center gap-8 w-4/5 text-positive p-2 text-xl">
					<li className="hover:scale-125">
						<Link href="/">Home</Link>
					</li>
					<li className="hover:scale-125">
						<Link href="/favorites">Favorites</Link>
					</li>
					<li className="hover:scale-125">
						<Link href="/characters">Characters</Link>
					</li>
				</ul>
			</header>
			{isOpen && (
				<div
					className={`absolute z-10 top-0 left-0 w-[100vw] h-full bg-cielo flex justify-between 
						${isOpen ? ' animate-slide-in-left' : 'animate-slide-out-left'}
					`}
				>
					<div className="w-[70%] bg-black px-4 py-8">
						<ul className="lg:hidden sx:flex flex-col justify-end items-start gap-8 w-full text-positive p-2 text-2xl ">
							<li className="hover:scale-125" onClick={() => setIsOpen(false)}>
								<Link href="/">Home</Link>
							</li>
							<li className="hover:scale-125" onClick={() => setIsOpen(false)}>
								<Link href="/favorites">Favorites</Link>
							</li>
							<li className="hover:scale-125" onClick={() => setIsOpen(false)}>
								<Link href="/characters">Characters</Link>
							</li>
						</ul>
					</div>
					<div className="flex flex-col">
						<Close onClick={() => setIsOpen(!isOpen)} color="white" size={'48'} className="cursor-pointer m-8" />
					</div>
				</div>
			)}
		</>
	)
}

export default Header
