'use client'

import React, { useState, useEffect } from 'react'
import CharacterCard from './characterCard'
import { ChevronLeft, ChevronRight } from '../icons'

type Props = {
	characters: Character[]
	filter?: string
}

export default function SearchPlanet({ characters, filter = '' }: Props) {
	const [shownCharacters, setShownCharacters] = useState<Character[] | []>([])
	const [currentPage, setCurrentPage] = useState(0)

	const itemsPerPage: number = 3
	const startIndex: number = currentPage * itemsPerPage
	const endIndex: number = startIndex + itemsPerPage

	useEffect(() => {
		const currentCharacters = characters?.slice(startIndex, endIndex)
		if (filter === '') {
			setShownCharacters(currentCharacters)
			return
		}
		setShownCharacters(currentCharacters?.filter(character => character.origin.name === filter))
	}, [filter, characters, startIndex, endIndex])

	const handleNextPage = () => {
		setCurrentPage(currentPage + 1)
	}

	const handlePrevPage = () => {
		setCurrentPage(currentPage - 1)
	}

	return (
		<React.Fragment>
			<div className="w-full mb-8 flex lg2:flex-row sx:flex-col justify-center items-center gap-8 ">
				<div className="lg2:w-[10%]">
					{currentPage !== 0 ? (
						<ChevronLeft
							onClick={handlePrevPage}
							className="lg2:block sx:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					) : (
						<ChevronLeft className="lg2:block sx:hidden text-secondary text-9xl w-36 h-36 z-40" />
					)}
				</div>

				<div className="lg:w-[80%] sx:w-full flex lg:flex-row sx:flex-col justify-center items-center gap-4 ">
					{shownCharacters?.map(char => {
						return (
							<div key={char.id} className="lg:w-[33%] sx:w-full lg:pt-[115px] sx:pt-12 flex justify-center">
								<CharacterCard
									id={char.id}
									status={char.status}
									type={char.type}
									gender={char.gender}
									species={char.species}
									image={char.image}
									name={char.name}
									origin={char.origin}
									location={char.location}
									episode={char.episode}
									url={char.url}
									created={char.created}
								/>
							</div>
						)
					})}
				</div>
				<div className="lg2:w-[10%]">
					{endIndex >= characters?.length ? (
						<ChevronRight className="lg2:block sx:hidden  text-secondary text-9xl w-36 h-36 z-40" />
					) : (
						<ChevronRight
							onClick={handleNextPage}
							className="lg2:block sx:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					)}
				</div>

				<div className="w-full sx:flex lg2:hidden justify-between items-center">
					{currentPage !== 0 ? (
						<ChevronLeft
							onClick={handlePrevPage}
							className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					) : (
						<ChevronLeft
							className={
								shownCharacters?.length !== 0
									? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					)}
					{endIndex >= characters?.length ? (
						<ChevronRight
							className={
								shownCharacters?.length !== 0
									? 'sx:block lg2:hidden text-secondary text-9xl w-36 h-36 z-40'
									: 'invisible'
							}
						/>
					) : (
						<ChevronRight
							onClick={handleNextPage}
							className="sx:block lg2:hidden text-green-600 text-9xl w-36 h-36 z-40 hover:scale-125 cursor-pointer"
						/>
					)}
				</div>
			</div>
		</React.Fragment>
	)
}
