import { useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { Button } from './button'
import { Star } from '../icons'
import { useAddFavorite } from '@/data/store'

export default function CharacterCard({ id, name, status, image }: Character) {
	const { addToFav, deleteToFav, favorites } = useAddFavorite()

	const [isInFavorites, setIsInFavorites] = useState(favorites.includes(id))

	const onFavoritesClick = () => {
		isInFavorites ? deleteToFav(id) : addToFav(id)
		setIsInFavorites(state => !state)
	}

	return (
		<div className="relative max-w-[290px] w-full min-h-[484px] bg-positive rounded-[40px] hover:scale-[1.02] transition-transform">
			<button className="absolute top-[10px] left-[10px] rounded-full overflow-hidden" onClick={onFavoritesClick}>
				<Star color={isInFavorites ? 'rgb(135, 245, 78)' : 'rgb(255, 255, 255)'} />
			</button>
			<div className="w-full h-[290px] rounded-[10px]">
				<Image src={image} alt={name} priority width={311} height={438} className=" object-contain rounded-t-[40px] " />
			</div>

			<Button
				className="bg-black absolute top-[55%] right-[20px] shadow-[0_0_0_3px_rgba(202,250,130,1)]"
				shape="round"
				size="large"
			>
				<Link href={`/characters/${id}`}>GO</Link>
			</Button>
			<div className="text-primary text-2xl p-4">
				<p className="font-bold">Name:</p>
				<p>{name}</p>
				<p className="font-bold">Status:</p>
				<p>{status}</p>
			</div>
		</div>
	)
}
