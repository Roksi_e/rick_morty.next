/* eslint-disable @next/next/no-img-element */
'use client'

// import Image from 'next/image'
import Link from 'next/link'

function Footer() {
	return (
		<footer className="w-full bg-gray-900 lg:h-24 sx:h-44 flex lg:flex-row sx:flex-col items-center gap-4 lg:py-2 sx:py-6 px-2  z-10 ">
			<div className="lg:h-[100%] sx:h-[50%] lg:w-1/5 sx:w-[80%] lg:mx-0 sx:mx-auto flex items-center justify-center lg:border-r-2 sx:border-r-0 border-white p-2">
				<img src={'/images/logo.png'} alt="logo" />
			</div>
			<ul className="flex lg:justify-start sx:justify-between lg:gap-8 sx:gap-4 lg:w-4/5 sx:w-[80%] lg:mx-0 sx:mx-auto text-white p-2 lg:text-xl sx:text-lg">
				<li className="hover:text-positive">
					<Link href="/">Home</Link>
				</li>
				<li className="hover:text-positive">
					<Link href="/favorites">Favorites</Link>
				</li>
				<li className="hover:text-positive">
					<Link href="/characters">Characters</Link>
				</li>
			</ul>
		</footer>
	)
}

export default Footer
