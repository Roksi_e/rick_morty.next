'use client'

import { useEffect, useState } from 'react'
import Link from 'next/link'
import Image, { StaticImageData } from 'next/image'
import { planetsAll } from '@/services/methods'

export default function PlanetCard({ id, name }: ILocation) {
	const [randomPlanet, setRandomPlanet] = useState<StaticImageData>(planetsAll[0])

	function randomItem() {
		let src
		let i = 0
		i = Math.floor(Math.random() * planetsAll.length)
		src = planetsAll[i]
		setRandomPlanet(src)
	}

	const randomSize = (min: number, max: number) => {
		return Math.floor(Math.random() * (max - min) + min)
	}

	useEffect(() => {
		randomItem()
	}, [])

	return (
		<div className=" w-[100%]  p-2 flex justify-center">
			<Link href={`/planet/${id}`} className="w-auto h-auto flex flex-col">
				<Image
					src={randomPlanet}
					alt="planet"
					priority
					style={{
						width: `${Math.floor(Math.random() * (300 - 10) + 10)}`,
						height: `${Math.floor(Math.random() * (300 - 10) + 10)}`
					}}
				/>

				<div className="hover:bg-black hover:bg-opacity-50  text-md min-h-[60px] p-2 text-transparent hover:text-white font-semibold inset-11">
					{name}
				</div>
			</Link>
		</div>
	)
}
